const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const products = require('./db');
const crypto = require('crypto');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
 * Helper structures doing some fake db/middleware lifting
 */
const byCat = new Map();
const byId = new Map();
const similarPrice = new Map();

const PAGING_LEN = 24;
const N_SIMILAR = 4;

/**
 * Sort and map out categories at startup and on insertions.
 */
function initProducts() {
  products.sort(function (a, b) {
    return a.price - b.price;
  });

  products.forEach((obj) => {
    if (byCat.has(obj.category)) byCat.get(obj.category).push(obj);
    else byCat.set(obj.category, [obj]);
  });

  products.forEach((obj) => {
    byId.set(obj.id, obj);
  });

  for (const k of byCat.keys()) {
    const cat = byCat.get(k);
    cat.forEach((obj, i, arr) => {
      // Iterate over a nearest-neighbor range of N*2 to not miss price cliff edges
      const clamp = (val) => Math.max(Math.min(val, arr.length - 1), 0);

      let remainderLeft = 0;
      let remainderRight = 0;
      const initialLeft = clamp(i - N_SIMILAR);
      const initialRight = clamp(i + N_SIMILAR);

      if (initialLeft == 0) remainderRight = Math.abs(N_SIMILAR - i);
      if (initialRight == arr.length - 1)
        remainderLeft = Math.abs(Math.abs(arr.length - 1 - i) - N_SIMILAR);

      const trueLeft = initialLeft - remainderLeft;
      const trueRight = initialRight + remainderRight;

      const similars = arr
        .slice(trueLeft, trueRight)
        .map((finalist) => {
          if (finalist.id !== obj.id) {
            let priceDiff = Math.abs(parseInt(finalist.price) - parseInt(obj.price));
            return {
              ...finalist,
              priceDiff: priceDiff,
            };
          }
        })
        .sort(function (a, b) {
          return a.priceDiff - b.priceDiff;
        })
        .slice(0, N_SIMILAR);
      similarPrice.set(obj.id, similars.map(similar => similar.id));
    });
  }
}

http.createServer(app).listen(3001, () => {
  console.log('Listen on 0.0.0.0:3001');
  initProducts();
});

app.get('/', (_, res) => {
  res.send({ status: 200 });
});

/**
 * Take 1 UUID, return N_SIMILAR nearest in price in same category.
 * @param id      one existing product ID
 * ex: GET /similar?id=78c2effc-8325-4a1b-9896-e072c2d9f225
 */
app.get('/similar', (req, res) => {
  try {
    const similarProducts = [
      ...similarPrice.get(req.query.id).map((id) => byId.get(id)),
    ];
    res.status(200).send(similarProducts);
  } catch {
    res.status(400).send('ID not found');
  }
});

/**
 * Requests for products
 * @param category  a valid product category
 * @param min       products should be >= this
 * @param max       products should be <= this
 * @param page      pagination @ PAGING_LEN results per return
 * ex: GET /products?category=meat&min=45&max=55&page=2
 */
app.get('/products', (req, res) => {
  let outgoing = [];
  const min =
    typeof req.query.min !== 'undefined'
      ? parseInt(req.query.min)
      : Number.MIN_SAFE_INTEGER;
  const max =
    typeof req.query.max !== 'undefined'
      ? parseInt(req.query.max)
      : Number.MAX_SAFE_INTEGER;

  try {
    if (typeof req.query.category !== 'undefined') {
      outgoing = byCat.get(req.query.category);
    } else {
      outgoing = products;
    }

    // TODO halt evaluation after max
    outgoing = outgoing.filter(function (obj) {
      return parseInt(obj.price) <= max && parseInt(obj.price) >= min;
    });

    // TODO actually clamp this stuff to result counts
    const startIndex =
      ((typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1) -
        1) *
      PAGING_LEN;
    const endIndex = startIndex + PAGING_LEN;

    res.status(200).send(outgoing.slice(startIndex, endIndex));
  } catch (e) {
    res.status(400).send();
  }
});

/**
 * Accept new product parameters, generates an ID and inserts into
 * in-memory product array. Does no validation.
 * @param name  product name
 * @param sep   category (meat, greens, fish)
 * @param trail price in whole kroner
 */
app.post('/add', (req, res) => {
  const uuid = crypto.randomUUID();
  const newProd = {
    id: uuid,
    name: req.body.name,
    category: req.body.category,
    price: req.body.price,
  };
  products.push(newProd);
  res.status(201).send({ id: uuid }); // not using for anything
  initProducts();
});

process.on('SIGINT', function () {
  process.exit();
});
