import { useState } from 'react';

const ep = 'http://0.0.0.0:3001/add';

function App() {
  const [name, setName] = useState('');
  const [category, setCategory] = useState('');
  const [price, setPrice] = useState('');
  const [message, setMessage] = useState('');

  let handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res = await fetch(ep, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name,
          category,
          price,
        }),
      });

      if (res.status === 201) {
        setName('');
        setCategory('');
        setPrice('');
        setMessage('Product added successfully');
      } else {
        setMessage('Something went wrong');
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="app">
      <h3>Add a food:</h3>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={name}
          placeholder="Name"
          onChange={(e) => setName(e.target.value)}
        />
        <select
          id="category"
          name="category"
          onChange={(e) => setCategory(e.target.value)}
          defaultValue="CATEGORY"
        >
          <option disabled>
            CATEGORY
          </option>
          {['meat', 'greens', 'fish'].map((name) => (
            <option key={name}>{name}</option>
          ))}
        </select>
        <input
          type="number"
          value={price}
          placeholder="Price"
          onChange={(e) => setPrice(e.target.value)}
        />
        <button type="submit">Create</button>
      </form>
      <div className="message">{message ? <p>{message}</p> : null}</div>
    </div>
  );
}

export default App;
